import React from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';

import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  // ADD YOUR FIREBASE CREDENTIALS
  apiKey: "AIzaSyBXU3fkag77XQ5c8JS89xit4Mv2U2ewd7c",
    authDomain: "find-tutor-6b23f.firebaseapp.com",
    databaseURL: "https://find-tutor-6b23f.firebaseio.com",
    projectId: "find-tutor-6b23f",
    storageBucket: "find-tutor-6b23f.appspot.com",
    messagingSenderId: "270332218313"
};

firebase.initializeApp(firebaseConfig);

import { Container, Content, Header, Form, Input, Item, Button, Label } from 'native-base'

export default class LoginScreen extends React.Component {

  constructor(props) {
    super(props)

    this.state = ({
      email: '',
      password: ''
    })
  }

  componentDidMount() {

    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        console.log(user)
      }
    })
  }

  signUpUser = (email, password) => {

    try {
      const { navigate } = this.props.navigation;
      firebase.auth().createUserWithEmailAndPassword(email, password).then(function (user) {
          console.log(user);
          navigate('HomeScreen');
        }).catch((error) =>
          {
          alert(error);
      });
    }
    catch (error) {
      console.log(error.toString())
    }
  }

  loginUser = (email, password) => {

    try {
      const { navigate } = this.props.navigation;
      firebase.auth().signInWithEmailAndPassword(email, password).then(function (user) {
          console.log(user);
          navigate('HomeScreen');

        }).catch((error) =>
          {
          alert(error);
      });
    }
    catch (error) {
      console.log(error.toString())
    }
  }

  async loginWithFacebook() {

    //ENTER YOUR APP ID
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1907126889604680', { permissions: ['public_profile'] })

    if (type == 'success') {

      const credential = firebase.auth.FacebookAuthProvider.credential(token)
      const { navigate } = this.props.navigation;
      firebase.auth().signInWithCredential(credential).then(function (user) {
          console.log(user);
      navigate('HomeScreen');
        }).catch((error) => {
        console.log(error)
      })
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Form>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input
              autoCorrect={false}
              autoCapitalize="none"
              onChangeText={(email) => this.setState({ email })}
            />

          </Item>

          <Item floatingLabel>
            <Label>Password</Label>
            <Input
              secureTextEntry={true}
              autoCorrect={false}
              autoCapitalize="none"
              onChangeText={(password) => this.setState({ password })}
            />
          </Item>

          <Button style={{ marginTop: 10 }}
            full
            rounded
            success
            onPress={() => this.loginUser(this.state.email, this.state.password)}
          >
            <Text style={{ color: 'white' }}> Login</Text>
          </Button>

          <Button style={{ marginTop: 10 }}
            full
            rounded
            primary
            onPress={() => this.loginWithFacebook()}
          >
            <Text style={{ color: 'white' }}> Login With Facebook</Text>
          </Button>



        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 10
  },
});
