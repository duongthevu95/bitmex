import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";
import { TabNavigator } from "react-navigation";
import ChatScreen from '../Screens/ChatScreen';
import FavouriteScreen from '../Screens/FavouriteScreen';
import BecomeTutorScreen from '../Screens/BecomeTutorScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import FindTutorScreen from '../Screens/FindTutorScreen';
import { Icon } from 'react-native-elements';
class FindTutor extends Component{
  render(){
    return(
      <FindTutorScreen />
    );
  }
}
class Chat extends Component{
  render(){
    return(
        <ChatScreen />
    );
  }
}
class BecomeTutor extends Component{
  render(){
    return(
        <BecomeTutorScreen />
    );
  }
}
class Favourite extends Component{
  render(){
    return(
        <FavouriteScreen />
    );
  }
}
class Profile extends Component{
  render(){
    return(
        <ProfileScreen />
    );
  }
}
const HomeScreenTabNavigator = TabNavigator({
  FindTutor: {
    screen: FindTutor,
    navigationOptions: {
      tabBarLabel: 'FindTutor',
      tabBarIcon: ({ tintColor }) => <Icon name="search" size={35} color={tintColor} />
    }
  },
  BecomeTutor: {
    screen: BecomeTutor,
    navigationOptions: {
      tabBarLabel: 'BecomeTutor',
      tabBarIcon: ({ tintColor }) => <Icon name="unarchive" size={35} color={tintColor} />
    }
  },
  Chat: {
    screen: Chat,
    navigationOptions: {
      tabBarLabel: 'Chat',
      tabBarIcon: ({ tintColor }) => <Icon name="chat" size={35} color={tintColor} />
    }
  },
  Favourite: {
    screen: Favourite,
    navigationOptions: {
      tabBarLabel: 'Favourite',
      tabBarIcon: ({ tintColor }) => <Icon name="favorite" size={35} color={tintColor} />
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    }
  }
},{
  animationEnabled:false,
  swipeEnabled: true,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showIcon: true,
    labelStyle: {
      //fontSize: 12
    },
    style: {
      backgroundColor: '#dddddd'
    },
    inactiveTintColor: 'green',
    activeTintColor: 'red',
  }
})
export default HomeScreenTabNavigator;
