import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {StackNavigator} from 'react-navigation';
import LoginScreen from './Screens/LoginScreen';
import FirstScreen from './Screens/FirstScreen';
import SignUpScreen from './Screens/SignUpScreen';
import HomeScreen from './Screens/HomeScreen';
export default class App extends React.Component {
  render() {
    return (
      <AppNavigator />
    );
  }

}
export const HomeScreenStack = StackNavigator({
  HomeScreen  : { screen : HomeScreen}
})
export const LoginStack = StackNavigator({
  FirstScreen : { screen : FirstScreen},
  LoginScreen : { screen : LoginScreen},
  SignUpScreen: { screen : SignUpScreen}
})
const AppNavigator = StackNavigator({
  FirstScreenStack  : { screen : LoginStack},
  HomeScreen  : { screen : HomeScreenStack}
}, {
  mode: 'modal',
  headerMode: 'none',
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
