import React, {Component} from "react";
import {View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Container, Content, Header, Form, Input, Item, Button, Label } from 'native-base'
class FirstScreen extends Component{
  render(){
     const { navigate } = this.props.navigation;
    return(
    <Container style={styles.container}>
    <Form>
          <Label>Login or Sign Up to find your best tutor</Label>
          <Button style={{ marginTop: 10 }} full rounded success onPress={() =>
            navigate('LoginScreen')}>
        <Text style={{ color: 'white' }}> Log In </Text>
        </Button>
        <Button style={{ marginTop: 10 }} full rounded primary onPress={() =>
          navigate('SignUpScreen')}>
        <Text style={{ color: 'white' }}> Sign Up </Text>
      </Button>
      </Form>
      </Container>
    );
  }
}
export default FirstScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 10
  }
})
